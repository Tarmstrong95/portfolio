import { unified } from "unified";
import markdown from "remark-parse";
import { wikiLinkPlugin } from "remark-wiki-link";
import html from "remark-html";
import { default as Node } from "./node";
import rehypePrism from "rehype-prism-plus";
import remarkRehype from "remark-rehype";
import rehypeStringify from "rehype-stringify";
import Utils from "../lib/utils";
import type { BackLink } from "./types.js";

export class Transformer {
  static haveFrontMatter = (content?: string): boolean => {
    if (!content) return false;
    const indexOfFirst = content.indexOf("---");
    if (indexOfFirst === -1) {
      return false;
    }
    const indexOfSecond = content.indexOf("---", indexOfFirst + 1);
    return indexOfSecond !== -1;
  };

  static getFrontMatterData = (_filecontent: string) => {
    return {};
  };

  static pageResolver = (pageName: string): string[] => {
    const allFileNames = Utils.getAllMarkdownFiles();
    const result = allFileNames.find((aFile) => {
      const parseFileNameFromPath = Transformer.parseFileNameFromPath(aFile);
      return (
        Transformer.normalizeFileName(parseFileNameFromPath) ===
        Transformer.normalizeFileName(pageName)
      );
    });

    if (result === undefined || result.length === 0) {
      // TODO: fix, does nothing for now
    }

    return result !== undefined && result.length > 0
      ? [Utils.toSlug(result)]
      : ["/"];
  };

  static hrefTemplate = (permalink: string): string => {
    // permalink = Transformer.normalizeFileName(permalink)
    permalink = permalink.replace("ç", "c").replace("ı", "i").replace("ş", "s");
    return `/notes/${permalink}`;
  };

  static getHtmlContent = (content: string): string[][] => {
    let htmlContent: string[] = [];
    const sanitizedContent = Transformer.preprocessThreeDashes(content);

    unified()
      .use(markdown, { gfm: true })
      .use(rehypePrism)
      .use(rehypeStringify)
      .use(remarkRehype)
      .process(sanitizedContent, function (err, file) {
        htmlContent.push(String(file).replace("\n", ""));
        if (err !== null && err !== undefined) {
          console.log("CUSTOM ERROR @transformer.js:89:" + err);
        } else {
          return;
        }
      });
    // .use(obsidianImage)
    // .use(highlight)
    // .use(externalLinks, { target: "_blank", rel: ["noopener"] })
    // .use(frontmatter, ['yaml', 'toml'])
    // .use(wikiLinkPlugin, {
    //   permalinks: null,
    //   pageResolver: (pageName) => {
    //     return Transformer.pageResolver(pageName);
    //   },
    //   hrefTemplate: (permalink) => {
    //     return Transformer.hrefTemplate(permalink);
    //   },
    //
    //   aliasDivider: "|",
    // })

    let joinedContent = htmlContent.join("");
    let splitAgainContent = joinedContent.split("---");
    return [splitAgainContent];
  };

  /* SANITIZE MARKDOWN FOR --- */
  static preprocessThreeDashes = (content: string): string => {
    const indexOfFirst = content.indexOf("---");
    if (indexOfFirst === -1) {
      return content;
    }
    const indexOfSecond = content.indexOf("---", indexOfFirst + 1);
    content.slice(0, indexOfSecond);
    const contentPart = content.slice(indexOfSecond);
    return contentPart.split("---").join("");
  };

  /* Normalize File Names */
  static normalizeFileName = (filename: string | null): string => {
    if (!filename) return "";
    let processedFileName = filename.replace(".md", "");
    processedFileName = processedFileName.replace("(", "").replace(")", "");
    processedFileName = processedFileName.split(" ").join("-");
    processedFileName = processedFileName.toLowerCase();
    return processedFileName;
  };

  /* Parse file name from path then sanitize it */
  static parseFileNameFromPath = (
    filepath: string | undefined,
  ): string | null => {
    if (typeof filepath !== "string") return null;
    if (!filepath.includes("/")) return null;

    const splitPath = filepath.split("/");
    const parsedFileFromPath = splitPath[splitPath.length - 1];
    return parsedFileFromPath.replace(".md", "");
  };

  /* Pair provided and existing Filenames */
  static getInternalLinks = (aFilePath: string): BackLink[] => {
    const fileContent = Node.readFileSync(aFilePath);
    const internalLinks: BackLink[] = [];
    const sanitizedContent = Transformer.preprocessThreeDashes(fileContent);
    unified()
      .use(markdown, { gfm: true })
      .use(wikiLinkPlugin, {
        pageResolver: function (pageName: string) {
          // let name = [Transformer.parseFileNameFromPath(pageName)];

          let canonicalSlug: string;
          if (pageName.includes("#")) {
            const tempSlug = pageName.split("#")[0];
            if (tempSlug.length === 0) {
              canonicalSlug = Utils.toSlug(aFilePath);
            } else {
              canonicalSlug =
                Transformer.pageResolver(tempSlug)[0].split("#")[0];
            }
          } else {
            canonicalSlug = Transformer.pageResolver(pageName)[0].split("#")[0];
          }

          const backLink: BackLink = {
            title: Transformer.parseFileNameFromPath(
              Utils.toFilePath(canonicalSlug),
            ),
            slug: canonicalSlug,
            shortSummary: canonicalSlug,
          };

          if (
            canonicalSlug != null &&
            !internalLinks.some((l) => l.slug == canonicalSlug)
          ) {
            internalLinks.push(backLink);
          }

          return [canonicalSlug];
        },

        hrefTemplate: (permalink: string) => {
          return Transformer.hrefTemplate(permalink);
        },

        aliasDivider: "|",
      })
      .use(html)
      .processSync(sanitizedContent);
    return internalLinks;
  };
}
