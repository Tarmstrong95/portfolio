
export function NotesPageContainer({ children }: { children: React.ReactNode[] }) {
  return (
    <div className='flex  w-full' style={{ height: 'inherit' }}>
      {children}
    </div>
  )
}
