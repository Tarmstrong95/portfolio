export const siteTitle = "Digital Backroom - An Internet Archive";
export default function Layout({ children }) {
  return (
    <div style={{ height: "100dvh" }}>
      <main style={{ height: "100dvh" }}>{children}</main>
    </div>
  );
}
