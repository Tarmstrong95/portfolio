import * as React from "react";
import { TreeView, TreeItem } from "@mui/x-tree-view";
import { useRouter } from "next/router";
import { styled } from "@mui/material/styles";

const TCTreeItem = styled(TreeItem)(({ theme }) => ({
  "& .MuiTreeItem-content": {
    borderRadius: "10px",
    maxWidth: 200,

    "&:hover": {
      backgroundColor: "rgba(25, 118, 210, 0.59)",
    },

    "& .MuiTreeItem-label": {
      fontSize: "1rem",
      paddingLeft: "6px",
      fontFamily:
        '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif,',
      lineHeight: 2.0,
      overflow: "hidden",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis",
    },
  },
}));

export default function FolderTree(props) {
  const router = useRouter();
  const memoTree = React.useMemo(() => renderTree(props.tree), [props.tree]);
  const memoExpandedIds = React.useMemo(() => [props.tree.id], [props.tree]);

  return (
    <>
      <TreeView
        aria-label="rich object"
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpanded={memoExpandedIds}
        defaultExpandIcon={<ChevronRightIcon />}
        onNodeSelect={(event, nodIds) => {
          const currentNode = binaryFind(props.flattenNodes, nodIds);
          if (!currentNode) return;
          if (!currentNode.routePath) return;
          router.push(currentNode.routePath);
        }}
      >
        {memoTree}
      </TreeView>
    </>
  );
}

function binaryFind(sourceList, valToFind) {
  let leftIndex = 0;
  let rightIndex = sourceList.length - 1;
  let middleIndex;
  while (rightIndex >= leftIndex) {
    middleIndex = leftIndex + Math.floor((rightIndex - leftIndex) / 2);
    const currentValue = sourceList[middleIndex];
    if (currentValue.id == valToFind) return currentValue;
    if (currentValue.id > valToFind) rightIndex = middleIndex - 1;
    else leftIndex = middleIndex + 1;
  }

  // We reach here when element is not
  // present in array
  return null;
}

function ChevronRightIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      height={10}
      width={10}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="m8.25 4.5 7.5 7.5-7.5 7.5"
      />
    </svg>
  );
}

function ExpandMoreIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      height={10}
      width={10}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="m19.5 8.25-7.5 7.5-7.5-7.5"
      />
    </svg>
  );
}

function renderTree(nodes, layer = 0) {
  if (nodes.name == "notes")
    return Array.isArray(nodes.children)
      ? nodes.children.map((n) => renderTree(n, layer + 1))
      : null;
  return (
    <TCTreeItem
      key={nodes.id}
      nodeId={nodes.id}
      label={nodes.name}
      sx={{ marginLeft: 1 * layer }}
    >
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => renderTree(node, layer + 1))
        : null}
    </TCTreeItem>
  );
}

FolderTree.Container = FolderTreeContainer;
function FolderTreeContainer({ children }) {
  return (
    <div
      className="y-scroll p-md bg-2"
      style={{
        height: "100%",
        minWidth: 150,
        maxWidth: 500,
        borderRight: "1px solid #fff1",
      }}
    >
      {children}
    </div>
  );
}
