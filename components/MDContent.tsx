import React from 'react'
import { useRouter } from 'next/router'

export interface LinkType {
  slug: string
  title: string
  shortSummary: string
}

interface BackLinksProps {
  linkList: LinkType[]
}

function BackLinks({ linkList }: BackLinksProps) {
  return (<div className="">
    <h3 className="">Link to this note</h3>
    {(linkList != null && linkList.length > 0)
      ? <>
        <div className="">
          {linkList.map(aLink =>
            <div key={aLink.slug} className="">
              {/* <Link href={aLink.slug}> */}
              <a href={aLink.slug}>
                <p className="">{aLink.title}</p>
                <p className="">{aLink.shortSummary} </p>
              </a>
              {/* </Link> */}
            </div>
          )}
        </div>
      </>
      : <> <p className=""> No backlinks found</p> </>}
  </div>)
}

interface MDContentProps {
  content: string
  backLinks: LinkType[]
}

function MDContent({ content, backLinks }: MDContentProps) {
  useRouter()

  return (

    <div className="flex-1 p-xl mw-0 maxw-sm bg-1">
      <div dangerouslySetInnerHTML={{ __html: content }} />
      <BackLinksGroup links={backLinks} />
      <hr />
      <footer>
        <p>Powered by  <a href="https://gitlab.com/Tarmstrong95/XeNote">XeNote</a>, © {new Date().getFullYear()}</p>
      </footer>
    </div>
  )
}

interface BackLinksGroupProps {
  links: LinkType[]
}

function BackLinksGroup({ links }: BackLinksGroupProps) {
  if (links?.length === 0) {
    return (
      <p className=""> No backlinks found</p>
    )
  }

  return (
    <details>
      <summary>
        BackLinks
      </summary>
      <BackLinks linkList={links} />
    </details>
  )
}

export default MDContent
