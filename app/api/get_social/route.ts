export interface ResponseData {
  title: string;
  body: string;
  images: Array<{
    url: string;
    width: number;
    height: number;
  }>;
  date: string;
}

export async function GET() {
  return Response.json({
    title: "Snow Day! ☃️ 🏂",
    date: new Date().toDateString(),
    body: "I didn't really want to go out today but the kids really wanted me to watch them slide on the snow. I actually ended up having a really good time!",
    images: [
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV84UPpPaP9ozI316hChoCMXtrZVeb1qRikOFajLk7WJW_gljKcivqNP3YJYHHE6ufLbvZbQIYG_RE99xp0qCZWtSTVhpGZXCrx9HAuuB6T31tuIhR51UHvpppdiV-vm-GcLggxf_s4HuKsAJtQWeXhiw=w2048-h1153-s-no-gm",
        width: 2048,
        height: 1153,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV86EDRl0oVrNXSoOOmsuAvGMA1ViwK-SMQCYGggmNUssp7M6yaULBqcWTBfdsMU1jQsd3TletSbhRMxl_W2g6qnk430pSgNk_5WZVOkHQxsBFowOxaob7OjbB_1V-1u7T98R56cs-Zq8PSWNI3rX9G44=w978-h1738-s-no-gm",
        width: 678,
        height: 1205,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV86MRCxFd16j_0l1IcylKJZxbgVOT0poIODKlo9iPoxvpQW8BVOfn41bKFMXtFjtEIPE6W2-YGqhmtXAVcUJVPznNqEUtAlhL0UbwUbdzVYMCARHWDGgccnr5ncBgIoMoSUggJjoSiMwD0_89jlu5rcv=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV84GCm8WKLj2Tk9AOTqLFXj1Ka5oGNqAjoq2hpmOcP24apI1SJB1GdpxDpl_s9cHgptrMU6641VX0jvWeIFtE-zfxOyW8xaUNgznqhBVbqbniEL39CWbhY4A7ZCQMmXCqOXfyEIPVXs1IqOe7f7eU0pY=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV861lS4nwvwQim0m1ttCvwUY0bK3WD2v2aWHoq47VcfmaLoJFjmuyplablTfAj3tow2Jp5OXQX6b1ctkyO4FPdvFBDkWvtjVb7Ogsuw6yK2qy79d5aMQoFEkgjwwqgKi79Yy1defsLPHVuyWqfN6JKEp=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV84UPpPaP9ozI316hChoCMXtrZVeb1qRikOFajLk7WJW_gljKcivqNP3YJYHHE6ufLbvZbQIYG_RE99xp0qCZWtSTVhpGZXCrx9HAuuB6T31tuIhR51UHvpppdiV-vm-GcLggxf_s4HuKsAJtQWeXhiw=w2048-h1153-s-no-gm",
        width: 2048,
        height: 1153,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV861lS4nwvwQim0m1ttCvwUY0bK3WD2v2aWHoq47VcfmaLoJFjmuyplablTfAj3tow2Jp5OXQX6b1ctkyO4FPdvFBDkWvtjVb7Ogsuw6yK2qy79d5aMQoFEkgjwwqgKi79Yy1defsLPHVuyWqfN6JKEp=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV861lS4nwvwQim0m1ttCvwUY0bK3WD2v2aWHoq47VcfmaLoJFjmuyplablTfAj3tow2Jp5OXQX6b1ctkyO4FPdvFBDkWvtjVb7Ogsuw6yK2qy79d5aMQoFEkgjwwqgKi79Yy1defsLPHVuyWqfN6JKEp=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV861lS4nwvwQim0m1ttCvwUY0bK3WD2v2aWHoq47VcfmaLoJFjmuyplablTfAj3tow2Jp5OXQX6b1ctkyO4FPdvFBDkWvtjVb7Ogsuw6yK2qy79d5aMQoFEkgjwwqgKi79Yy1defsLPHVuyWqfN6JKEp=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV84UPpPaP9ozI316hChoCMXtrZVeb1qRikOFajLk7WJW_gljKcivqNP3YJYHHE6ufLbvZbQIYG_RE99xp0qCZWtSTVhpGZXCrx9HAuuB6T31tuIhR51UHvpppdiV-vm-GcLggxf_s4HuKsAJtQWeXhiw=w2048-h1153-s-no-gm",
        width: 2048,
        height: 1153,
      },
      {
        url: "https://lh3.googleusercontent.com/pw/ABLVV861lS4nwvwQim0m1ttCvwUY0bK3WD2v2aWHoq47VcfmaLoJFjmuyplablTfAj3tow2Jp5OXQX6b1ctkyO4FPdvFBDkWvtjVb7Ogsuw6yK2qy79d5aMQoFEkgjwwqgKi79Yy1defsLPHVuyWqfN6JKEp=w994-h1766-s-no?authuser=0",
        width: 944,
        height: 1766,
      },
    ],
  });
}
