import {
  Html,
  Head,
  Main,
  NextScript,
  type DocumentProps,
} from "next/document";

interface CustomDocumentProps extends DocumentProps {
  bodyClass?: string
}

export default function Document(props: CustomDocumentProps) {
  const pageProps = props?.__NEXT_DATA__?.props?.['pageProps'];

  return (
    <Html>
      <Head>
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/gh/kimeiga/bahunya/dist/bahunya.min.css"
        />
      </Head>

      <body className={pageProps?.bodyClass ? pageProps.bodyClass : ''}>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
