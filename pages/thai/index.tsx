import Head from 'next/head'
import { useRef, useState } from 'react';

const cardSpinning = [
  { transform: "rotate3D(0, 1, 0, 360deg)" },
]

const cardSpinningTimer = {
  duration: 500,
  iterations: 1,
};

export default function Home() {
  return (
    <>
      <Head>
        <title>Thai Stuff</title>
      </Head>

      <div style={{
        display: 'grid',
        gridTemplateColumns: 'repeat(5, 1fr)',
        gridTemplateRows: 'repeat(2, 1fr)',
        width: '100%',
        gap: 8
      }}>
        <FlippyCard />
        <div className='grey-round' style={{ gridColumn: '2 / span 4' }}>
          <p></p>
        </div>

        <div style={{ gridRow: 2, gridColumn: '1 / span 5' }}>
          <div className='grey-round' style={{ gridColumn: '1 / span 5' }}>
            <p></p>
          </div>
        </div>
      </div>
    </>
  )
}

function FlippyCard() {
  const cardRef = useRef<HTMLDivElement>(null)
  const [randomeLetter, setRandomLetter] = useState<LetterType>({
    letter: 'ฮ',
    phono: 'ho ho',
  })

  async function _getRandomLetter() {
    cardRef.current?.animate(cardSpinning, cardSpinningTimer);

    await (new Promise((res, rej) => {
      setTimeout(() => {
        res(true)
      }, 100);
    }))

    return thaiConsonantsArray[Math.floor(Math.random() * thaiConsonantsArray.length)]
  }

  return (
    <div className='card grey-round' ref={cardRef}>
      <div>
        <p>{randomeLetter.letter}</p>
        <p>{randomeLetter.phono}</p>
      </div>
      <div className='buttons'>
        <button onClick={async () => {
          console.log("pressed listen")
        }}>Listen</button>
        <button onClick={async () => {
          let k = await _getRandomLetter()
          while (k == randomeLetter) {
            k = await _getRandomLetter()
          }
          setRandomLetter(k)
        }}>Next</button>
      </div>
    </div>
  )
}


const thaiConsonantsArray: LetterType[] = [
  {
    letter: 'ก',
    phono: 'ko kai',
  },
  {
    letter: 'ข',
    phono: 'kho khai',
  },
  {
    letter: 'ค',
    phono: 'kho khon',
  },
  {
    letter: 'ง',
    phono: 'ngo ngoo',
  },
  {
    letter: 'จ',
    phono: 'cho chan',
  },
  {
    letter: 'ฉ',
    phono: 'so chan',
  },
  {
    letter: 'ช',
    phono: 'so chon',
  },
  {
    letter: 'ซ',
    phono: 'so san',
  },
  {
    letter: 'ฌ',
    phono: 'so cho',
  },
  {
    letter: 'ญ',
    phono: 'yo ning',
  },
  {
    letter: 'ฎ',
    phono: 'do dek',
  },
  {
    letter: 'ฏ',
    phono: 'to tao',
  },
  {
    letter: 'ฐ',
    phono: 'tho thon',
  },
  {
    letter: 'ฑ',
    phono: 'tho thung',
  },
  {
    letter: 'ฒ',
    phono: 'tho tho',
  },
  {
    letter: 'ณ',
    phono: 'no nen',
  },
  {
    letter: 'บ',
    phono: 'bo bai',
  },
  {
    letter: 'ป',
    phono: 'po pao',
  },
  {
    letter: 'พ',
    phono: 'pho phan',
  },
  {
    letter: 'ม',
    phono: 'mo mi',
  },
  {
    letter: 'ฝ',
    phono: 'fo fan',
  },
  {
    letter: 'ฟ',
    phono: 'fo fa',
  },
  {
    letter: 'ภ',
    phono: 'pho phran',
  },
  {
    letter: 'ฝ',
    phono: 'fo fan',
  },
  {
    letter: 'ส',
    phono: 'so san',
  },
  {
    letter: 'ศ',
    phono: 'so si',
  },
  {
    letter: 'ษ',
    phono: 'so so',
  },
  {
    letter: 'ส',
    phono: 'so san',
  },
  {
    letter: 'ห',
    phono: 'ho hin',
  },
  {
    letter: 'ฬ',
    phono: 'lo ling',
  },
  {
    letter: 'ฮ',
    phono: 'ho ho',
  },
];

interface LetterType {
  letter: string,
  phono: string,
}
