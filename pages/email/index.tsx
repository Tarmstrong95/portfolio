import Layout from "components/Layout";
import Head from "next/head";

export default function EmailPageRoot() {
  return (
    <>
      <Head>
        <title>Email Signup</title>
      </Head>
      <Layout>

        <div style={{ maxWidth: 500, margin: 'auto', minWidth: 0 }}>
          <h4 style={{ textAlign: 'center' }}>Please provide your Name & Email</h4>

          <br />

          <div className="bg-elevated p-lg rounded-sm frosted">
            <form className="flex-col">
              <label htmlFor="name">Name</label>
              <input type="text" name="Name" id="name" placeholder="John Doe" />
              <label htmlFor="email">Email</label>
              <input type="email" name="Email" id="email" placeholder="johndoe@email.com" />
              <br />
              <button className="flex-1 p-md">Add Me!</button>
            </form>
          </div>
        </div>
      </Layout >

      <div className="bubble" />
      <div className="bubble" />
      <div className="bubble" />
    </>
  )
}

export function getStaticProps() {
  return {
    props: {
      body_class_name: 'h-full'
    }
  }
}
