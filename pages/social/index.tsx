import type { ResponseData } from "app/api/get_social/route";
import Head from "next/head";
import Image from "next/image";
import { useEffect, useState } from "react";

export default function Home() {
  const [data, setData] = useState<ResponseData | null>(null)
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    fetch('/api/get_social')
      .then((res) => res.json())
      .then((data) => {
        setData(data as ResponseData)
        setLoading(false)
      })
  }, [])

  if (isLoading) return <p>Loading...</p>
  if (!data) return <p>No profile data</p>

  return (
    <>
      <Head>
        <title>Triston Armstrong | Life Updates</title>
      </Head>

      <div className="flex-col x-center p-md" style={{ marginTop: '3rem' }}>
        <div className="maxw-sm border rounded-sm p-lg">
          <Avatar />
          <h4>Welcome! 👋🏻</h4>
          <p>Thanks for stopping by! Here you can find my life updates!</p>
          <p>I will be working on sending updates via email when this page updates. But in the meantime feel free to check in</p>
        </div>
      </div>

      <div className="flex-col x-center  mb-4 p-md" >
        <PostCard {...{ data }} />
      </div>
    </>
  );
}


function PostCardHeader({ title, date }: Pick<ResponseData, 'title' | 'date'>) {
  return (
    <>
      <h5 className="m-0">{title}</h5>
      <small className="font-muted"> {date}</small>
      <hr />
    </>
  )
}

function PostCardDescription({ body }: Pick<ResponseData, 'body'>) {
  return (
    <p>{body}</p>
  )
}

function PostCardImageGrid({ images }: Pick<ResponseData, 'images'>) {
  let outterImages;
  if (images.length > 4) {
    outterImages = images.slice(4 - images.length)
    images.splice(4, images.length - 4)
  }

  return (
    <div className="gridContainer">
      {images.map(picture => {
        const style = {
          flexShrink: 0,
          gridColumnEnd: `span ${getSpanEstimate(picture.width / 3)}`,
          gridRowEnd: `span ${getSpanEstimate(picture.height / 3)}`,
        }
        return (
          <div style={style} className="rounded-sm flex y-center x-center hide-overflow">
            <Image style={{
              minWidth: '100%',
              minHeight: '100%',
            }} alt="profile image" src={picture.url} width={0} height={0} />
          </div>
        )
      })}

      {!!outterImages && (
        <div style={{
          flexShrink: 0,
          gridColumnEnd: `span ${getSpanEstimate(1)}`,
          gridRowEnd: `span ${getSpanEstimate(1)}`,
        }} className="rounded-sm flex y-center x-center hide-overflow">
          <Image onClick={() => alert("yeaaaa you cant do that yet.. sorry")} style={{
            filter: 'blur(4px)',
            minWidth: '100%',
            minHeight: '100%',
          }} alt="profile image" src={outterImages[0].url} width={0} height={0} />
          <div style={{
            position: 'absolute',
            fontWeight: 'bold',
            color: 'black',
            fontSize: '2rem'
          }}>+{outterImages.length}</div>
        </div>
      )}
    </div>
  )
}

function PostCard({ data }: { data: ResponseData }) {
  const { title, date, body, images } = data

  return (
    <div className="maxw-sm border rounded-sm p-lg">
      <PostCardHeader {...{ title, date }} />

      <PostCardDescription {...{ body }} />

      <PostCardImageGrid {...{ images }} />

      <hr />

      <div className="flex-row w-all flex-between">
        <small className="font-muted">Made with ❤️  & ☕️</small>
        <small className="font-muted">All rights reserved</small>
      </div>
      {/*
          <div className="flex w-all">
            <input placeholder="Comment..." className="flex-1" />
            <button>Send</button>
          </div>
          */}
    </div>
  )
}









export function getStaticProps() {

  return {
    props: {

    }
  }
}

function getSpanEstimate(size: number) {
  if (size > 500) {
    return 2
  }

  return 1
}


function Avatar() {
  return (
    <Image className="rounded-sm" style={{
      width: '100%',
      // height: '230px',
      objectFit: 'cover',
      // objectPosition: '0 -140px'
    }} alt="profile image" src={'https://lh3.googleusercontent.com/pw/ABLVV865I8CCefefHzMOr4BAz3OupkBkU7hUETbAgn2vXuuD6bU9karalr5UgFaJDSL9J3rK0-qCZUI0o3jrhQxj8wt7MxyHVsbrN8QXYn_gSrP4wp4XSoT3PNcBlLXq8DjkyuMnh6D0OHf_E4t0X1ToZuDK=w959-h960-s-no?authuser=0'} width={50} height={50} />

  )
}
