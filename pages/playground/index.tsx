import Head from 'next/head'

export default function Home() {
  return (
    <>
      <Head>
        <title>🛝 Playground</title>
      </Head>

      <h1>Playground</h1>
      <article>I may play with some ideas here</article>
    </>
  )
}
