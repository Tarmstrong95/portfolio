import Job, { type JobProps } from '../components/portfolio/Job'
import Skill, { type SkillProps } from '../components/portfolio/Skill'
import ListItem, { type ListItemProps } from '../components/portfolio/ListItem'
import { UsefulLinksList } from '../components/UsefulLinks/index'
import { SocialLinksList } from '../components/SocialLinks/SocialLinks'
import type { GetStaticProps } from 'next'
import Head from 'next/head'

interface LandingProps {
  jobs: JobsType[]
  skills: SkillsType[]
  projects: ProjectsType[]
}

export default function Landing({ jobs, skills, projects }: LandingProps): React.JSX.Element {
  return (
    <>
      <Head>
        <title>Triston Armstrong</title>
      </Head>

      <div className='flex-col x-center'>
        <header>
          <article className='flex-row align-baseline'>
            <p>❗👷 This site is under construction 🚧 🏗️ 🚧</p>
          </article>

          <br />

          <br />

          <div className="flex-col gap-05">

            <h1 className='m-0'>Triston Armstrong 🫰</h1>
          </div>

          <p>
            Full Stack Software Developer {' '}
            <a target="_blank" href="https://ventrahealth.com">
              @VentraHealth
            </a>
          </p>

          <p className='font-muted'>
            I am a self taught Full Stack Software Developer with an unhealthy addiction to solving
            problems using code.
          </p>

          <hr />
        </header>

        <main className='w-all'>
          <section>
            <h4>☝🏻 Shortcuts <i className='font-muted'><small>(Shortcuts to some of my other pages)</small></i></h4>
            <div className="flex gap-1" id="skills">
              <a href="/notes" target="_blank" className=''>Notes</a>
              <a href="/social" target="_blank" className=''>Social</a>
              <a href="/playground" target="_blank" className=''>Playground</a>
              <a href="/thai" target="_blank" className=''>Thai Playground</a>
            </div>
          </section>

          <section>
            <h4>🤹 Skills <i className='font-muted'><small>(Click me to fast track to docs)</small></i></h4>
            <div className="flex gap-1" id="skills">
              {skills.map(skill => <Skill key={skill.label} label={skill.label} link={skill.link} />)}
            </div>
          </section>

          <section>
            <h4>💼 Jobs</h4>
            <div className="flex-col " id="jobs">
              {jobs.map(job => <Job key={job.name} logo={job.logo} name={job.name} details={job.details} dateFrom={job.dateFrom} dateTo={job.dateTo} />)}
            </div>
          </section>

          <section>
            <h4>🚧 Projects</h4>
            <div className='flex-col'>
              {projects.map(project => <ListItem key={project.label} label={project.label} date={project.date} url={project.url} />)}
            </div>
          </section>
        </main>

        <footer className='flex-col flex-between gap-1 mt-4 '>
          <div className='flex-row gap-1'>
            <UsefulLinksList />
          </div>

          <div style={{ margin: '10px 0' }} />

          <div className='flex-row flex-between '>
            <div className='flex-row gap-1'>
              <SocialLinksList />
            </div>
            <small>©2023-2024 Triston Armstrong. All rights reserved.</small>
          </div>

        </footer>
      </div>
    </>
  )
}

export function getStaticProps(): ReturnType<GetStaticProps<{
  projects: ProjectsType[]
  skills: SkillsType[]
  jobs: JobsType[]
}>> {
  return {
    props: {
      projects: [
        { label: 'Solar Battery Monitor API', date: 'July \'23', url: 'https://github.com/tristonarmstrong/SolarBatteryMonitorApi' },
        { label: 'Armstrong Editor', date: 'Dec. \'22', url: 'https://github.com/tristonarmstrong/armstrong-editor' },
        { label: 'Web Window Manager', date: 'Sept. \'22', url: 'https://github.com/tristonarmstrong/web-window-manager' },
        { label: 'Component Test Helper', date: 'June \'22', url: 'https://github.com/tristonarmstrong/component-test-helper' },
        { label: 'Hive DAPP', date: 'Mar. \'22', url: 'https://github.com/tristonarmstrong/hive-dapp' },
        { label: 'Kivy Twisted Input Capture', date: 'June \'21', url: 'https://github.com/tristonarmstrong/KivyTwistedInputCapture' },
        { label: 'Plant Monitor Node MCU', date: 'June \'21', url: 'https://github.com/tristonarmstrong/PlantMonitorNodeMCU' },
        { label: 'Oppo BDP 103 CLI', date: 'Apr. \'21', url: 'https://github.com/tristonarmstrong/oppo_bdp_103_CLI' },
        { label: 'Sony Bravia Pro Server', date: 'Apr. \'21', url: 'https://github.com/tristonarmstrong/sony_bravia_pro_display_mock_server' },
        { label: 'Chat IO', date: 'Jun. \'20', url: 'https://github.com/tristonarmstrong/chat.io' },
        { label: 'Zip Code Distance App', date: 'Jan. \'20', url: 'https://github.com/tristonarmstrong/zipapp' }
      ],
      skills: [
        { label: 'Typescript', link: 'https://www.typescriptlang.org/docs/handbook/intro.html' },
        { label: 'Python', link: 'https://docs.python.org/3/' },
        { label: 'Rust', link: 'https://doc.rust-lang.org/std/index.html' },
        { label: 'Javascript', link: 'https://developer.mozilla.org/en-US/docs/Web/javascript#reference' }
      ],
      jobs: [
        {
          logo: 'https://ventrahealth.com/wp-content/uploads/2023/08/cropped-Ventra-Health-favicon-circle-192x192.png',
          name: 'Ventra Health',
          details: 'Maintaining and iterating on an internal web application',
          dateFrom: 'May \'23',
          dateTo: 'Present'
        }, {
          logo: 'https://www.randstadusa.com/themes/custom/bluex/favicon.ico',
          name: 'Randstad Technologies',
          details: 'Built Web Applications for external clients',
          dateFrom: 'May \'22',
          dateTo: 'May \'23'

        }, {
          logo: 'https://img1.wsimg.com/isteam/ip/9ba626a3-41c9-4092-90b5-cb351983b726/favicon/a8beec51-e35d-4cca-8e88-befa12f687b0.png/:/rs=w:64,h:64,m',
          name: 'Damiano Global Corporation',
          details: 'Built Web Applications for external clients',
          dateFrom: 'July \'20',
          dateTo: 'Nov. \'21'

        }, {
          logo: 'images/makers_logo.png',
          name: 'Makers Ladder LLC',
          details: 'Did some thangs',
          dateFrom: 'Dec. \'19',
          dateTo: 'Apr. \'22'

        }
      ]
    }
  }
}

type SkillsType = SkillProps
type JobsType = JobProps
type ProjectsType = ListItemProps
