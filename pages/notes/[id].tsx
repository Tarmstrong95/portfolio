import Head from 'next/head'
import Layout from 'components/Layout'
import Util from 'lib/utils'
import FolderTree from 'components/FolderTree'
import MDContent, { type LinkType } from 'components/MDContent'
import type { GetStaticProps } from 'next/types'
import type { ParsedUrlQuery } from 'querystring'
import { NotesPageContainer } from 'components/NotesPageContainer'

interface HomeProps {
  note: { title: string, data: string }
  fileNames: string[]
  content: string
  tree: Record<string, unknown>
  flattenNodes: unknown[]
  backLinks: LinkType[]
}

export default function Home({ note, backLinks, fileNames: _, tree, flattenNodes }: HomeProps) {
  return (
    <Layout>
      <Head>
        {note.title && <meta name="title" content={note.title} />}
      </Head>
      <NotesPageContainer>
        <FolderTree.Container>
          <br />
          <FolderTree tree={tree} flattenNodes={flattenNodes} />
        </FolderTree.Container>

        <MDContent content={note.data} backLinks={backLinks} />
      </NotesPageContainer>
    </Layout>
  )
}

export async function getStaticPaths() {
  const allPostsData = Util.getAllSlugs()
  const paths = allPostsData.map(p => ({ params: { id: p } }))

  return {
    paths,
    fallback: false
  }
}

export const getStaticProps: GetStaticProps = (context) => {
  const { nodes, edges }: { nodes: unknown[], edges: unknown[] } = Util.constructGraphData()
  const { id } = context.params as ParsedUrlQuery & { id: string }

  const note = Util.getSinglePost(id)
  const tree = Util.convertObject(Util.getDirectoryData())
  const flattenNodes = Util.getFlattenArray(tree)

  const listOfEdges: unknown[] = edges.filter(anEdge => (anEdge as { target: string }).target === id)
  const internalLinks: unknown[] = listOfEdges.map((anEdge) => nodes.find(aNode => (aNode as { slug: string }).slug === (anEdge as { source: string }).source)).filter(element => element !== undefined)
  const backLinks = [...new Set(internalLinks)]
  return {
    props: {
      note,
      tree,
      flattenNodes,
      backLinks: backLinks.filter((link) => (link as { slug: string }).slug !== id),
      bodyClass: 'm-0'
    }
  }
}
