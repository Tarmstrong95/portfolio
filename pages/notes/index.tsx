import Layout from 'components/Layout'
import Util from 'lib/utils'
import FolderTree from 'components/FolderTree'
import MDContent, { type LinkType } from 'components/MDContent'
import { NotesPageContainer } from 'components/NotesPageContainer'

export interface HomeProps {
  content: string
  tree: Record<string, unknown>
  flattenNodes: unknown[]
  backLinks: LinkType[]
}

export default function Home({ content, tree, flattenNodes, backLinks }: HomeProps) {
  return (
    <Layout>
      <NotesPageContainer>
        <FolderTree.Container>
          <br />
          <FolderTree tree={tree} flattenNodes={flattenNodes} />
        </FolderTree.Container>
        <MDContent content={content} backLinks={backLinks} />
      </NotesPageContainer>
    </Layout >
  )
}

export function getStaticProps() {
  const { nodes, edges }: { nodes: unknown[], edges: unknown[] } = Util.constructGraphData()
  const dirData = Util.getDirectoryData()
  const tree = Util.convertObject(dirData)
  const contentData = Util.getSinglePost('index')
  const flattenNodes = Util.getFlattenArray(tree)
  // const listOfEdges = edges
  //   .filter((anEdge) => (
  //     anEdge as { target: string }).target === 'index'
  //   )
  // const internalLinks = listOfEdges.map(
  //   anEdge => nodes
  //     .find(
  //       aNode => (
  //         aNode as { slug: string }).slug === (anEdge as { source: string }).source))
  //   .filter(
  //     element => element !== undefined)
  //const backLinks = [...new Set(internalLinks)]

  return {
    props: {
      content: contentData.data,
      tree,
      flattenNodes,
      bodyClass: 'm-0'
      // backLinks,
    }
  }
}
